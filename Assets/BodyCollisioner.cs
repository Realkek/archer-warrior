﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyCollisioner : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "ArrowSample(Clone)")
        {
            transform.parent.parent.GetComponent<Animator>().SetInteger("currentState", 2);
            Destroy(GetComponent<BoxCollider>());
            Destroy(GetComponent<SphereCollider>());
            Destroy(GetComponent<Rigidbody>());

            if (gameObject.name == "HeadCollider")
            {
                Destroy(transform.parent.parent.GetChild(1).GetComponent<Rigidbody>());
                Destroy(transform.parent.parent.GetChild(1).GetComponent<BoxCollider>());
            }

            if (gameObject.name == "Group1")
            {
                Destroy(transform.parent.parent.GetChild(0).GetComponent<Rigidbody>());
                Destroy(GetComponent<SphereCollider>());
            }

            transform.parent.parent.GetComponent<SimpleAI>().isPursueAllowed = false;
            transform.parent.parent.GetComponent<SimpleAI>().isPossiblePursue = false;
            StartCoroutine(DisableEnemyBot());
        }
    }

    IEnumerator DisableEnemyBot()
    {
        yield return new WaitForSeconds(1.5f);
        transform.parent.parent.gameObject.SetActive(false);
    }
}