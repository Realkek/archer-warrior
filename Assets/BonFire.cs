﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonFire : MonoBehaviour
{
    public GameObject gameOverPanel;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Horse&Player")
        {
            other.GetComponent<Animator>().SetInteger("State", 0);
            gameOverPanel.SetActive(true);
        }
    }
}