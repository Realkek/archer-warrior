﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class VisionCollisioner : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Horse&Player")
        {
            transform.parent.GetComponent<SimpleAI>().instance.isPursueAllowed = true;
            transform.parent.GetComponent<Animator>().SetInteger("currentState", 1);
        }
    }
}