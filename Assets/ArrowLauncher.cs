﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowLauncher : MonoBehaviour
{
    // [SerializeField] private GameObject ArrowsHub;
    public ArrowLauncher instance;
    private float _speed = 0.8f;
    private Vector3 startArrowDefaultPosition;
    private bool isLaunched;
    private bool _isFlightCleared;
    private bool _isArrowCharged;

    private GameObject destinationDot;


    void Start()
    {
        instance = this;
        startArrowDefaultPosition = transform.localPosition;
        ManagerUpdate.AddTo(this);
    }

    // public void Tick()
    // {
    //     FlyArrow();
    // }

    public void SetIsFlyCleared()
    {
        _isFlightCleared = true;
    }

    public void SetIsNotFlyCleared()
    {
        _isFlightCleared = false;
    }

    public void SetIsArrowCharged()
    {
        _isArrowCharged = true;
    }

    public void SetIsNotArrowCharged()
    {
        _isArrowCharged = false;
    }

    public void SetDestinationCoordinates()
    {
        transform.localPosition = startArrowDefaultPosition;
        transform.parent = transform.root;
        destinationDot = transform.GetChild(0).gameObject;
        ManagerEvents.CheckTriggeringEvent("launched");
    }

    // private void FlyArrow()
    // {
    // }

    private void FixedUpdate()
    {
        if (_isFlightCleared == false && isLaunched == false)
            transform.localPosition = startArrowDefaultPosition;
        // if (_isArrowCharged && _isFlightCleared)
        // {
        //     rb.AddRelativeForce(-transform.forward * thrust);
        // }

        if (_isArrowCharged && _isFlightCleared)
        {
            isLaunched = true;
            transform.position = Vector3.MoveTowards(transform.position, destinationDot.transform.position, _speed);
        }
    }
}