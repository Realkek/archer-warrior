﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingFenceCollisioner : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "ArrowSample(Clone)")
        {
            Destroy(GetComponent<BoxCollider>());
            transform.GetComponent<Animation>().Play("MovingFence");
        }
    }
}