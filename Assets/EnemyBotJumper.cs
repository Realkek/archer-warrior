﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBotJumper : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Horse&Player")
        {
            transform.parent.GetComponent<Animator>().SetInteger("currentState", 3);
            transform.parent.GetComponent<SimpleAI>().instance.isPossiblePursue = false;
        }
    }
}