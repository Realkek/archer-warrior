﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowRecharger : MonoBehaviour, IEventTrigger, ITick, IEventSub
{
    [SerializeField] private GameObject chargingArrow;
    private GameObject _chargingArrow;
    private static int _arrowId;
    private Touch _touch;
    private int _currentArrowNumber;
    public AudioManager audioPlayer;

    private void Start()
    {
        Subscribe();
        StartCharging();
        ManagerUpdate.AddTo(this);
    }

    IEnumerator Recharge()
    {
        transform.GetComponent<Animator>().SetInteger("blo", 2);

        yield return new WaitForSeconds(0.4f);
        // transform.GetComponent<Animator>().SetInteger("State", 0);
        _arrowId++;
        _chargingArrow = Instantiate(chargingArrow, transform);
        // _chargingArrow.name = $"ChargingArrow{_arrowId}";
        _currentArrowNumber = _arrowId;
        _chargingArrow.GetComponent<ArrowLauncher>().instance.SetIsArrowCharged();
        // TriggerEvent($"ArrowCharged{_chargingArrow.name}");
    }

    private void StartCharging()
    {
        StartCoroutine(Recharge());
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }


    public void Tick()
    {
        if (Input.touchCount > 0)
        {
            _touch = Input.GetTouch(0);
            if (_touch.phase == TouchPhase.Ended)
            {
                if (_currentArrowNumber == _arrowId)
                {
                    _chargingArrow.GetComponent<ArrowLauncher>().instance.SetDestinationCoordinates();
                    _chargingArrow.GetComponent<ArrowLauncher>().instance.SetIsFlyCleared();
                    audioPlayer.AudioSource.Play();
                    StartCharging();
                }
            }
        }


        // if (Input.anyKeyDown)
        //     if (_currentArrowNumber == _arrowId)
        //     {
        //         _chargingArrow.GetComponent<ArrowLauncher>().instance.SetDestinationCoordinates();
        //         _chargingArrow.GetComponent<ArrowLauncher>().instance.SetIsFlyCleared();
        //         audioPlayer.AudioSource.Play();
        //         StartCharging();
        //     }
    }

    private void PrepareCharge()
    {
        transform.GetComponent<Animator>().SetInteger("blo", 2);
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("launched", PrepareCharge);
    }

    public void UnSubscribe()
    {
        ManagerEvents.StopListening("launched", PrepareCharge);
    }
}