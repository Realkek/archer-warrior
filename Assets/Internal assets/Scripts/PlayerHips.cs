﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHips : MonoBehaviour
{
    [SerializeField] private Animation hipsRidingAnimation;

    private void Start()
    {
        hipsRidingAnimation.Play();
    }
}