﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorseMover : MonoBehaviour, IEventSub, ITick
{
    private Animator _horseAnimator;

    private int _trueDisableCounter;
    private int _trueEnableCounter;
    private bool _movementTrigger;

    // Start is called before the first frame update
    void Start()
    {
        ManagerUpdate.AddTo(this);
        _horseAnimator = gameObject.GetComponent<Animator>();
        Subscribe();
        _horseAnimator.SetInteger("State", 0);
    }


    private void StartMovement()
    {
        if (_trueEnableCounter < 10 && _movementTrigger == true)
        {
            _horseAnimator.SetInteger("State", 1);
            _trueEnableCounter++;
        }
    }

    private void StoptMovement()
    {
        if (_trueDisableCounter < 10 && _movementTrigger == false)
        {
            _horseAnimator.SetInteger("State", 0);
            _trueDisableCounter++;
        }
    }

    private void EnableMovementTrigeer()
    {
        _movementTrigger = true;
    }

    public void Subscribe()
    {
        ManagerEvents.StartListening("PlayerTouchMovedToMainScreen", EnableMovementTrigeer);
    }

    public void UnSubscribe()
    {
    }

    public void Tick()
    {
        StoptMovement();
        StartMovement();
    }
}