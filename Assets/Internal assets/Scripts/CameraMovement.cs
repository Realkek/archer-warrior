﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour, ITickFixed
{
    [SerializeField] private GameObject horse;
    [SerializeField] private GameObject currentCamera;
    private Vector3 _offset;
    private Vector3 _previousHorsePosition;

    // Start is called before the first frame update
    void Start()
    {
        ManagerUpdate.AddTo(this);
    }


    public void TickFixed()
    {
        if (Math.Abs(_previousHorsePosition.z) > 0 || Math.Abs(_previousHorsePosition.x) > 0)
            _offset = horse.transform.position - _previousHorsePosition;
        ;
        currentCamera.transform.position = new Vector3(currentCamera.transform.position.x + _offset.x,
            currentCamera.transform.position.y, currentCamera.transform.position.z + _offset.z
        );
        _previousHorsePosition = horse.transform.position;
    }
}