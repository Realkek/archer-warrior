﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMovedStartTrigger : MonoBehaviour, IEventTrigger, ITick
{
    [SerializeField] private GameObject handTouchMovedCursor;

    private Touch _touch;

    private void Awake()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        if (Input.touchCount > 0)
        {
            _touch = Input.GetTouch(0);

            if ((Input.anyKey || _touch.phase == TouchPhase.Moved) && gameObject.activeSelf == enabled)
            {
                gameObject.GetComponent<Animator>().Play("HideMainMenuPanelElements");
                handTouchMovedCursor.transform.GetComponent<Animator>().Play("HideCursorImage");
            }
        }
    }

    public void GameStartNotify()
    {
        handTouchMovedCursor.SetActive(false);
        TriggerEvent("PlayerTouchMovedToMainScreen");
        gameObject.SetActive(false);
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName, arguments);
    }
}