﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMovedRotator : MonoBehaviour, ITick, IEventTrigger
{
    [SerializeField] private GameObject horse;
    private Touch _touch;

    private Vector2 _touchPosition;

    private Quaternion _rotationY;

    private float rotateSpeedModifier = 0.22f;

    private void Awake()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        if (Input.touchCount > 0)
        {
            _touch = Input.GetTouch(0);

            if (_touch.phase == TouchPhase.Moved)
            {
                _rotationY = Quaternion.Euler(0f, _touch.deltaPosition.x * rotateSpeedModifier, 0f);
                if (horse.transform.rotation.y > -90f && horse.transform.rotation.y < 90f)
                    horse.transform.rotation = _rotationY * transform.rotation;
            }
        }
    }

    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}