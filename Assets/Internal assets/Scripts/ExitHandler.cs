﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitHandler : MonoBehaviour, ITick
{
    private void Start()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
}