﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelComplete : MonoBehaviour
{
    public GameObject LevelCompletePanel;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Horse&Player")
        {
            StartCoroutine(Completer(other));
            other.GetComponent<Animator>().SetInteger("State", 0);
            LevelCompletePanel.SetActive(true);
        }
    }


    IEnumerator Completer(Collider collisioner)
    {
        yield return new WaitForSeconds(1f);
        collisioner.GetComponent<Animator>().SetInteger("State", 0);
        LevelCompletePanel.SetActive(true);
    }

    public void GoMainScene()
    {
        SceneManager.LoadScene("GamePlay");
    }
}