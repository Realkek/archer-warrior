﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStickManCollisioner : MonoBehaviour
{
    public GameObject gameOverPanel;
    public float thrust = 100.0f;
    public GameObject line;
    public GameObject audios;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().name == "Group1" || other.GetComponent<Collider>().name == "HeadCollider")
        {
            audios.SetActive(false);
            line.SetActive(false);
            transform.parent.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            transform.parent.GetComponent<Animator>().SetInteger("State", 2);
            StartCoroutine(Completer(other));
        }
    }

    IEnumerator Completer(Collider collisioner)
    {
        yield return new WaitForSeconds(1.2f);
        transform.parent.parent.GetComponent<Animator>().SetInteger("State", 0);
        gameOverPanel.SetActive(true);
    }
}