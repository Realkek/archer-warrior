﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCollideHandler : MonoBehaviour, IEventTrigger
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Horse&Player" && other.name != "VisionCollider" && other.name != "JumpCollider" &&
            other.name != "PlayerTors")
        {
            TriggerEvent("ArrowCollided");
            // transform.parent = other.transform.parent.GetChild(1).transform;

            if (other.name == "Group1")
                transform.parent = other.transform.parent.parent.GetChild(2).transform;
            if (other.name == "HeadCollider")
                transform.parent = other.transform.parent.parent.GetChild(2).GetChild(2).GetChild(0).GetChild(0)
                    .GetChild(1)
                    .GetChild(0).transform;
            Destroy(GetComponent<BoxCollider>());
            gameObject.GetComponent<ArrowLauncher>().instance.SetIsNotFlyCleared();

            // if (other.name == "CubeByArrowSample")
            //     gameObject.SetActive(false);
        }
    }


    public void TriggerEvent(string eventName, params object[] arguments)
    {
        ManagerEvents.CheckTriggeringEvent(eventName);
    }
}