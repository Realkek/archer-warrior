﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorseCollisioner : MonoBehaviour
{
    public GameObject gameOverPanel;

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.name == "Group1" || other.collider.name == "HeadCollider")
        {
            other.transform.parent.parent.GetComponent<Animator>().SetInteger("State", 0);
            gameOverPanel.SetActive(true);
        }
    }
}