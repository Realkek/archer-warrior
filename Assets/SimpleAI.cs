﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SimpleAI : MonoBehaviour, ITickFixed
{
    NavMeshAgent myNavMeshAgent;
    [SerializeField] private GameObject player;
    public SimpleAI instance;
    public bool isPursueAllowed;
    public bool isPossiblePursue;
    private int _invision;

    private void Awake()
    {
        myNavMeshAgent = GetComponent<NavMeshAgent>();
        _invision = Random.Range(0, 2);
        if (_invision == 0)
            gameObject.SetActive(false);
    }

    void Start()
    {
        ManagerUpdate.AddTo(this);
        isPossiblePursue = true;
        instance = this;
    }


    public void TickFixed()
    {
        if (isPursueAllowed && isPossiblePursue)
            myNavMeshAgent.SetDestination(player.transform.position);
    }
}